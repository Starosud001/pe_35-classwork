"use strict";

/*
адание 1.

Получить и вывести в консоль следующие элементы страницы:


По идентификатору (id): элемент с идентификатором list;

По классу — элементы с классом list-item;

По тэгу — элементы с тэгом li;

По CSS селектору (один элемент) — третий li из всего списка;

По CSS селектору (много элементов) — все доступные элементы li.

Вывести в консоль и объяснить свойства элемента:

innerText;

innerHTML;

outerHTML. 
*/
/*
const list = document.getElementById("list"); //поиск элемента по id
console.log(list);

const list2 = document.getElementsByClassName("list-item");
console.log(list2); 

const listItems = document.getElementsByTagName("li");
console.log(listItems)

const list3 = document.querySelector(".list-item:nth-child(3)"); //получили третитй элемент селектора
console.log(list3)

const listItems3 = document.querySelectorAll("li");
console.log(listItems3);
*/

/*
Задание 2.

Получить элемент с классом .remove.
Удалить его из разметки.

Получить элемент с классом .bigger.
Заменить ему CSS-класс .bigger на CSS-класс .active.

Условия:

Вторую часть задания решить в двух вариантах: в одну строку и в две
строки.

 <ul class="list">
        <li class="list-item remove">Item 1</li>
        <li class="list-item">Item 2</li>
        <li class="list-item">Item 3</li>
        <li class="list-item">Item 4</li>
        <li class="list-item bigger">Item 5</li>
        <li class="list-item">Item 6</li>
        <li class="list-item">Item 7</li>
        </ul>
*/
/*
const remove= document.querySelector(".remove");
remove.remove();
console.log(remove);

const bigger = document.querySelector(".bigger");
bigger.classList.replace("bigger", "active");
console.log(bigger.classList);
console.log(bigger.className);
*/


/*
Задание 3.

На экране указан список товаров с указанием названия и количества на складе.

Найти товары, которые закончились и:

Изменить 0 на «закончился»;

Изменить цвет текста на красный;

Изменить жирность текста на 600.

Требования:

Цвет элемента изменить посредством модификации атрибута style.

<ul class="store">
<li>Сыр: 5</li>
<li>Вода: 16</li>
<li>Кофе: 0</li>
<li>Масло: 102</li>
<li>Чай: 13</li>
<li>Молоко: 0</li>
<li>Мёд: 10</li>
</ul>

*/
/*

let liElements = Array.from(document.querySelectorAll('.store li'));

liElements.forEach(element => {
    let split = +element.textContent.split(":")[1];
    
    if(split == 0) {
        split = "zakon4ilos";
        element.textContent = element.textContent.replace("0", "zakon4ilos")
        element.style.color = "red";
    }   
    
});
*/


/**

Задание 4.

Получить элемент с классом .list-item.
Отобрать элемент с контентом: «Item 5».

Заменить текстовое содержимое этого элемента на ссылку, указанную в секции
«дано».

Сделать это так, чтобы новый элемент в разметке не был создан.

Затем отобрать элемент с контентом: «Item 6».
Заменить содержимое этого элемента на такую-же ссылку.

Сделать это так, чтобы в разметке был создан новый элемент.

Условия:

Обязательно использовать метод для перебора;

Объяснить разницу между типом коллекций: Array и NodeList. 
*/

/*
/_ Дано _/ const targetElement =
'Google
it!';

*/
/*
<ul class="list">
<li class="list-item">Item 1</li>
<li class="list-item">Item 2</li>
<li class="list-item">Item 3</li>
<li class="list-item">Item 4</li>
<li class="list-item">Item 5</li>
<li class="list-item">Item 6</li>
<li class="list-item">Item 7</li>
</ul>
*/

const targetElement = '<a href="https://www.google.com" target="_blank" rel="noreferrer noopener">Google it!</a>';
let liElements=Array.from(document.querySelectorAll("list-item"));

const fifthItem = liElements.find((el) => el.textContent === "item 5");
fifthItem.innerHTML = targetElement;

