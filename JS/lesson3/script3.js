"Use strict"

//var someVAriable = 3;



/*
someVariable = [1,2,3,4,5];
console.log (typeof someVariable);



someVAriable = function () {};
console.log(typeof someVariable);




someVariable = { name: "Uasya", age: 18 };
console.log(typeof someVariable);
*/




//someVariable = { name: "Uasya", age: 18 } 
//instanceof Object;/*instanceof проверЯет на принадлежность к */
//console.log(typeof someVariable);



/*String to number*/
/*
console.log (+ "18a");              //NaN
console.log (parseFloat ("18a"));   //18
console.log (parseInt ("18a"));     //18
console.log (Number("18a") );       //NaN
*/



/*Boolean to Number*/
/*
console.log (+ true);               //1
console.log (parseFloat (true));    //NaN
console.log (parseInt (true));      //NaN
console.log (Number(false) );       //0
*/

/*null to number*/
/*
console.log (+ null);               //0
console.log (parseFloat (null));    //NaN
console.log (parseInt (null));      //NaN
console.log (Number(null) );        //0
*/


/*undefined*/
/*
console.log (+ undefined);              //NaN
console.log (parseFloat (undefined));   //NAn
console.log (parseInt (undefined));     //NaN
console.log (Number(undefined) );       //NaN
*/

/*Array to Number*/
/*
console.log (+ [undefined,+ 1, true, null]);            //NaN
console.log (parseFloat ([undefined, 1, true, null]));  //NaN
console.log (parseInt ([undefined, 1, true, null]));    //NaN   
console.log (Number([undefined, 1, true, null]) );      //NaN
console.log (Number([]) );                              // 0
*/


/*function to number*/
/*
console.log (+ function fnName () {});              //NaN
console.log (parseFloat (function fnName () {}));   //NaN
console.log (parseInt (function fnName () {}));     //NaN
console.log (Number(function fnName () {}) );       //NaN
*/

/*Object to Number*/
/*
console.log (+ {name:"uasya"});             //NaN
console.log (parseFloat ({name:"uasya"}));  //NaN
console.log (parseInt ({name:"uasya"}));    //NaN
console.log (Number({name:"uasya"}) );      //NaN
*/


/*to String*/
/*
let age = 33;
console.log(`${age}`);      //33
console.log(String(age));   //33
console.log((22).toString); //toString() { [native code]
console.log(`${true}`);     //true
console.log(String(true));  //true
*/

/*
console.log(`${null}`);     //null
console.log(String(null));  //null

console.log(`${undefined}`);    //undefined
console.log(String(undefined)); //undefined

console.log(`${[1, 2, null, undefined]}`)       //1,2,,
console.log(String([1, 2, null, undefined]));   //1,2,,


console.log(`${function fnName() {}}`);     //function fnName() {}
console.log(String({name:"uasya"}));        //[object Object]
*/

/*to Boolean*/
/*
console.log(Boolean(1));                    //true
console.log(!1);                            //false
console.log(!!1);                           //true
console.log(!!"1");                         //true
console.log("");                            
console.log(!![1, 2, 3]);                   //true
console.log(!!{age:1232});                  //true
console.log(!!null);                        //false
console.log(!!undefined);                   //false
console.log(!!function fnName () {});       //true
/*

