"Use strict"

/*
Задание 1.

Пользователь вводит в модальное окно любое число.
В консоль вывести сообщение:

Если число чётное, вывести в консоль сообщение «Ваше число чётное.»;

Если число не чётное, вывести в консоль сообщение «Ваше число не чётное.»;

Если пользователь ввёл не число, вывести новое модальное окно с сообщением
«Необходимо ввести число!».

Если пользователь во второй раз ввёл не число, вывести сообщение: «
Ошибка! Вы ввели не число.». 
*/

//моё решение начальное
/*
let number = prompt("введите число");

if (isNaN (number) ) {
    console.log("Ошибка! Вы ввели не число");
} if (number === "") {
    console.log("loh");
} else{
number = number % 2 === 0 ? console.log("Ваше число чётное") : console.log("Ваше число не чётное");
}
*/




//вариант с изменнением и рсширением (с группой)
/*
let number = prompt("введите число");

if (isNaN (number) || number === "" || number == null || number ==undefined) { 
    //использовали или в условии
    console.log ("Необходимо ввести число");
}else{
        number = number % 2 === 0 ? console.log("Ваше число чётное") : console.log("Ваше число не чётное");
        }
*/

//для повторного запроса как в ТЗ
/*
    number = prompt("введите число");
    if (isNaN (number) || number === "" || number == null || number ==undefined) {
        alert ("Ошибка! Вы ввели не число");
} else{
number = number % 2 === 0 ? console.log("Ваше число чётное") : console.log("Ваше число не чётное");
}*/

/*
Задание 2.

Написать программу, которая будет приветствовать пользователя.
Сперва пользователь вводит своё имя, после чего программа выводит в консоль
сообщение с учётом его должности.

Список должностей:
Mike — CEO;
Jane — CTO;
Walter — программист:
Oliver — менеджер;
John — уборщик.

Если введёно не известное программе имя — вывести в консоль сообщение
«Пользователь не найден.».

Выполнить задачу в двух вариантах:

используя конструкцию if/else if/else;

используя конструкцию switch. */

/*
let name = prompt("Your name");


//вариант со Switch

switch(name){

    case "Mike":
        console.log("hello " + name + " ceo");
        break;
        
        case "Jane":
        alert ("hello " + name + " CTO");
        break;

        case "Walter" :
        alert ("hello " + name + " программист");
        break;

        case "Oliver":
        alert ("hello " + name + " уборщик");
        break;

        case "John":
        alert ("hello " + name + " менеджер");
        break;
}
*/

//вариант с if
/*
if (name == "Mike"){
    alert ("hello " + name + " ceo");
} else{
    if (name == "Jane") {
        alert ("hello " + name + " CTO");
} else { 
    if(name == "Walter"){
        alert ("hello " + name + " программист");
} else {
    if(name == "Oliver"){
        alert ("hello " + name + " уборщик");
} else {
    alert ("пользователь не нден")
}ай
}}}
*/

/*
Задание 3.

Пользователь вводит 3 числа.
Вывести в консоль сообщение с максимальным числом из введённых.

Если одно из введённых пользователем чисел не является числом,
вывести сообщение: «⛔️ Ошибка! Одно из введённых чисел не является числом.».

Условия: объектом Math пользоваться нельзя. 
*/
/*
let x =prompt("num");
let y =prompt("num");
let z =prompt("num");


if(
    isNaN(x || y ||z) || x == "" || y == "" || z == "" ) {
    alert("nont num");
}
else{ 
    if(x > y && x > z) {
    console.log(x);
    }
    else if (y > x && y > z) {
        console.log(y);
        }
        else {
            console.log(z);
            }
        }
        */