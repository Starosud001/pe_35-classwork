"use strict";
/*
const btn = document.getElementById("btn");
console.log(btn);
*/
/*
btn.onclick = () => { //при нажатии на кнопку в диалоговом окне всплывёт "Hi"
    alert("Hi");
};
*/

/*
btn.onclick = () => {
    alert("Hi again )");
    };
*/
/*
//   (addEventListener) - метод HTML который добавляет к объекту наблюдателя события
btn.addEventListener("click", () => { //первый аргумент это ожидаемое событие ("click").второгй аргумент функция
alert("Hi");
    });
*/
/*
 const btnClickHandler = () => {    //создали функцию  btnClickHandler
    alert("Hi again )");
    btn.removeEventListener("click", btnClickHandler);  // удалили воизбежание повтореня
    };
btn.addEventListener("click", btnClickHandler);
*/


/*
const onBtnClick = function (event) {
alert("Hi again 2)");
console.log(event);
console.log(this);
console.log(event.target);
console.log(this === event.target);
    };
btn.addEventListener("click", onBtnClick);
*/
// console.log(new Event("click"));
/*
document.addEventListener("DOMContentLoaded", function (evt) {});
*/
//DOMContentLoaded это событие означает что весь HTML страницы загружен

