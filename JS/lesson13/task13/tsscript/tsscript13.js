"use strict";
/*
Задание 1.
Написать скрипт, который создаст элемент button с текстом «Войти». При клике по
кнопке выводить alert с сообщением: «Добро пожаловать!».
Callback — это функция, которая срабатывает в ответ на событие. Совсем как в
сервисом с функцией «перезвоните мне». Мы оставляем заявку «перезвоните мне» —
это событие. Затем специалист на перезванивает (calls back).
*/

/*
const btn = document.createElement("button");//  
btn.textContent = "«Войти»"
document.body.prepend(btn)
btn.onclick = () => { //при нажатии на кнопку в диалоговом окне всплывёт "Hi"
    alert("«Добро пожаловать!»");
};

const onMouseover = function (){
    alert("«При клике по кнопке вы войдёте в систему.»")
    btn.removeEventListener("mouseenter", onMouseover);// удалится при первой отработке
}
btn.addEventListener("mouseenter", onMouseover)
*/
/*
Задание 2.
Улучшить скрипт из предыдущего задания. При наведении на кнопку указателем мыши,
выводить alert с сообщением: «При клике по кнопке вы войдёте в систему.».
Сообщение должно выводиться один раз.
Условия:

Решить задачу грамотно.

const onMouseover = function (){
    alert("«При клике по кнопке вы войдёте в систему.»")
    btn.removeEventListener("mouseenter", onMouseover);
}
btn.addEventListener("mouseenter", onMouseover)

*/




/*
Задание 3.

Создать элемент h1 с текстом «Добро пожаловать!». Под элементом h1 добавить
элемент button c текстом «Раскрасить». При клике по кнопке менять цвет каждой
буквы элемента h1 на случайный.

/_ Дано _/ const PHRASE = 'Добро пожаловать!';
function getRandomColor() { const r = Math.floor(Math.random() _ 255); const g =
Math.floor(Math.random() _ 255); const b = Math.floor(Math.random() * 255);
return rgb(${r}, ${g}, ${b}); }
*/



function getRandomColor() { 
    const r = Math.floor(Math.random() * 255); 
    const g = Math.floor(Math.random() * 255); 
    const b = Math.floor(Math.random() * 255);
    return `rgb(${r}, ${g}, ${b})`; }

    const h1 = document.createElement("h1");
document.body.prepend(h1)
h1.textContent = "«Добро пожаловать!»";
const button = document.createElement("button");
button.textContent = "«Добро пожаловать!»";
h1.after(button);//  
console.log(h1,button)

const changeColor = function () {
    let hArray = h1.textContent.split("").map(function (element) {
       let span = document.createElement("span");
       span.textContent = element;
       span.style.color = getRandomColor();
       return span;
    });
    h1.innerHTML = "";
 
    // hArray.forEach((element) => {
    //    h.append(element);
    // });
 
    h1.append(...hArray);//применили spred оператор (преобразил массив в набор элементов)
 };
 
 button.addEventListener("click", changeColor);
 

