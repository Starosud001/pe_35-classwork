"use strict";


//for 

/*
for(let i = 0; i < 20 ; i ++){
    if(true){
    console.log("hello world" + (i + 1));
}} 
*/

//while loop
/*
let number = +prompt("number");
while(number === 3){
    console.log("cool number");
    number = +prompt("number");
}
*/

//do while
/*
let number ;
do {
    number = +prompt("number");
    console.log("cool number");

} while(number === 3);
*/

//break
/*
label1: //метка
for(let i = 0; i < 500; i++) {
    console.log(i);
    if (i > 455) {
        continue label1; //цикл продолжит считать но прекратиться выполнение console.log("end loop")
    }     
    console.log("end loop");
}

console.log("end label1 loop")
*/

/*
let agree = false;
count = 0;

while (agree) { //не выполнит ни разу - условие ровно false
    console.log (count ++ +1);
    if(count >=10){
        agree =false;
    }
}
*/

/*
let agree = true;
count = 0;

while (agree) {// выполнит условие ровно true
    console.log (count ++ +1);
    if(count >=10){
        agree =false;
    }
}
*/

/*
do {
    console.log (count ++ +1);
    if(count >=10){
        agree =false;
}}
*/


//CONTINUE BREAK
/*
let agree = true;
let count = 0;

while (agree) {
    console.log (count ++ +1);
    if(count > 5){
        break;
        //continue; c continue код не дойдёт до следующего if
        // круг будет прерываться и идти с начала!!!
    }
    if (count >=10){
        agree = false;
    }
}
*/


//CONTINUE BREAK
/*
let agree = true;
let count = 0;

while (agree) {
    console.log (count ++ +1);
    if(count < 5){
        continue;
        //continue; пока count < 5 из-за continue код не дойдёт до следующего действия
        // круг будет прерываться и идти с начала а после 5 начн'т выполнять  console.log ("23 line")!!!
    }
    console.log ("23 line")
    if (count >=10){ 
        agree = false;// сдесь код остановится
    }
}
*/

