"use strict";
/*
Задание 1.
Написать программу для напоминаний.

Все модальные окна реализовать через alert.

Условия:
Если пользователь не ввёл сообщение для напоминания — вывести alert с сообщением
«Ведите текст напоминания.»;

Если пользователь не ввёл значение секунд,через сколько нужно вывести
напоминание — вывести alert с сообщением «Время задержки должно быть больше
одной секунды.»;

Если все данные введены верно, при клике по кнопке «Напомнить» необходимо её
блокировать так, чтобы повторный клик стал возможен после полного завершения
текущего напоминания; 

После этого вернуть изначальные значения обоих полей;

Создавать напоминание, если внутри одного из двух элементов input нажать клавишу
Enter; После загрузки страницы установить фокус в текстовый input.
<section>
    <h1>Напоминалка</h1>
    <div>
    <label for="reminder">Напомнить</label>
    <input type="text" value="" placeholder="Напомнить..." id="reminder" />
    </div>
    <div>
    <label for="seconds">Через</label>
    <input type="number" value="0" id="seconds" />
    <label for="seconds">секунд</label>
    </div>
    <button>Напомнить!</button>
    </section>
  */
const reminder = document.getElementById('reminder')
const seconds = document.getElementById('seconds')
const btn = document.getElementById('button')

function creatReminder (){
    btn.disabled = true
    if(reminder.value.trim() === ""){
        alert('введите текст')
        
    } 
    if (seconds.value <= 2){
        alert('«Время задержки должно быть большеодной секунды.»')
        return false
    }
    const timer = setTimeout(function(){
        alert(reminder.value);
        clearTimeout(timer);

    } ,seconds.value * 1000)
};

btn.addEventListener('click',creatReminder)
