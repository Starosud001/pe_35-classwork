"use strict";

// function summ(a = 0, b = 0) {
//    return a + b;
// }

// const summ = (a, b) => {
//    // to do
//    return a + b;
// };
// const summ = (a, b) => a + b;

// const getObj = () => ({ name: "Uasya" });

// const user = {
//    name: "Uasya",
//    getName: () => {
//       return this.name;
//    },
// };

// console.log(user.getName());

// const summ = (a = 0, b = 0) => a + b;
// console.log(summ(3));

// function summ() {
//    let res = 0;
//    for (const value of arguments) {
//       res += value;
//    }
//    return res;
// }
// console.log(summ(1, 2, 3, 4, 5, 6));

/////////////////////

// const summAllNums = (...params) => { // rest  в 39 строке получим  [1, 2, 3, 4, 5, 6] 
//    console.log(params);  //[1, 2, 3, 4, 5, 6] 
//    console.log(...params); // spred - получим просто значения 1, 2, 3, 4, 5, 6
// };
// console.log(summAllNums(1, 2, 3, 4, 5, 6));
/*
 const user = {
    name: "Uasya",
 };

const user2 = { ...user, age: 18 };
console.log(user2);   // rest    {name: "Uasya", age: 18}
*/

/*
 const innerArr = ["a", "b", "c"];
 console.log(innerArr);   
 console.log([1, 2, 3, 4, 5, innerArr]); // [1, 2, 3, 4, 5, Array(3)]
 console.log([1, 2, 3, ...innerArr, 4, 5]);  //spred  [1, 2, 3, "a", "b", "c", 4, 5] убрали скобки массива innerArr и поместили его значения в объемлющий его массив
*/