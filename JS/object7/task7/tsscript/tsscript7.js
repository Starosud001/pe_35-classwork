"use strict";
/*

Задание 1.


С помощью функции создать объект пользователя, который обладает тремя
свойствами:

Имя;

Фамилия;

Профессия.

А также одним методом sayHi, который выводит в консоль сообщение 'Привет.'.
*/

/*
Задание 2.

Расширить функционал объекта из предыдущего задания:

Метод sayHi должен вывести сообщение: «Привет. Меня зовут ИМЯ ФАМИЛИЯ.»;


Добавить метод, который меняет значение указанного свойства объекта.

Продвинутая сложность:
Метод должен быть «умным» — он генерирует ошибку при совершении попытки
смены значения несуществующего в объекте свойства. 
*/

/*
адание 3.

Расширить функционал объекта из предыдущего задания:

Добавить метод, который добавляет объекту новое свойство с указанным
значением.

Продвинутая сложность:
Метод должен быть «умным» — он генерирует ошибку при создании нового свойства
свойство с таким именем уже существует. 

Copy prev function
add method createProp
call method, take value -> add new prop with value
OK google -> как проверить существует ли свойство в обьекте
*/


function creatUser () {
    return {name: "Ivan" ,  //в это момент функция создала объект
        lastName: "Kuz", 
        job: "konstructor", 
        sayHi: function () { //создали метод (метод это встроенная функция)
        console.log (`привет, меня зовут ${this.name} ${this.lastName} `);      
},
        changeProperty: function(propertyName, propertyValue){ //задача 3
        if(Object.hasOwnProperty.call(this, propertyName)) //метод перебора по ключу
            this[propertyName] = propertyValue;
            else{
            console.log("Yeah")
        }
        },
        creatProperty: function (propertyName, propertyValue){
            if(!Object.hasOwnProperty.call(this, propertyName)) //метод перебора по ключу c инверсией (!)
            this[propertyName] = propertyValue;
            else{
            console.log("Oops")
        }
        },
        }
}



const user = creatUser();
//user.changeProperty("los", 'job'); //аргументы функции creatUser  (propertyName == "los"   propertyValue == 'job')
user.creatProperty("lastName", "22") //получим Oops (propertyName == lastName) (TASK3)
console.log(user);
//user.sayHi(); //task1
//creatUser().sayHi(); //тоже самое, что и в 30-40 строчках только напрямую


