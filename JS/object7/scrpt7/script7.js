"use strict";

    const user = {
    name: "Uasya",
    _age: 18,

    set age(value) {
        if (value > 0) {
            this._age = value;
        }
    },
    get age() {
        return this._age;
    },

    family: {
        father: {
            name: "Uasiliy",
            getName: function () {
                return this.name;
            },
        },
        mother: "Uasilisa",
    },
    getName: function () {
        return this.name;
    },
};


    //console.log(user["family"].father.getName.call(user));  //Uasya
    //user.age = 30; // задали возраст
 //console.log(user.age);  //30
 //console.log(user.name);  //Uasya

 //console.log(user);   //
 //console.log(user.getName());//Uasya

 //user.maxAge = 100;//новое свойство со значением
 //console.log(user);//

 //const user2 = user;//передали ссылку
//при изменениии user 2 бедет меняться и user

//console.log(user.name);
//user2.name = "Vitalic";
//user.name = "Uasya";
//console.log(user.name);
//console.log(user2.name);

 const user2 = {};// создали пустой объект

 for (const key in user) {
    user2[key] = user[key];//перенесли все значения из исходного в новый объкт
 }

// console.log(user2);
// console.log(user.name);
// user2.name = "Vitalic";

// console.log(user.name);
// console.log(user2.name);

 console.log(user2["getName"]());//отобразит значение получаемое функцией
 console.log(`${user2["getName"]}`); //отобразит функцию(конструкцию), которая срабатывает
 user.count = 1;
 user2.length = 33;
 const user3 = Object(user2);//свойство length = 33;перейдёт и в user3

 console.log(user3);

