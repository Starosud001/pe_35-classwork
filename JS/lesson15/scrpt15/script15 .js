"use strict";
/*
const list = document.getElementById("list");
const items = document.getElementsByClassName("list-item");
const body = document.body;

const clickHandler = function (event){
  //console.log(`'click on ${event.target.tagName}`)
  console.log(`'click on ${this}, ${event.target.tagName}`)
}
*/

/*
//увидим порядок прохождения действия (при (true) сверху вниз ,при (false) наоборот)
Array.from(items).forEach((el) => {
  el.addEventListener("click", clickHandler,true);
})
body.addEventListener("click",clickHandler,true)
list.addEventListener("click",clickHandler,true)
*/


/*
body.addEventListener("click", (event) => {//отобразится в консоле 3-им, после добавления аргумента (true) отобразится в консоле 1-ым
  console.log(`'click on body`)})//,true)

items.addEventListener("click", (event) => {  //отобразится в консоле 1-ым, после добавления аргумента (true) отобразится в консоле 3-им
  console.log(`'click on items`)},true)
*/


/*
list.addEventListener("click", //отобразится в консоле 2-ым , после добавления аргумента (true-) отобразится в консоле 2-ым
(event) => {  
  console.log(`'click on list`);
    console.log(event)
      event.stopPropagation;
        event.target.style.fontWeight =700}
          ,true);
*/


/*
//вариант " того что выше"
Array.from(items).forEach((el) =>{
  el.addEventListener(
    "click",
      (event) => {  
        console.log(`'click on items`);
          event.target.style.fontWeight = 700;}, //при каждом нажатии будет становится больше шрифт 
            true);
                  })
*/



// const clickHandler = function (evt) {
//    console.log("click on ");
//    console.log(this);
//    console.log(evt.target);
// };

// Array.from(items).forEach((el) => {
//    el.addEventListener("click", clickHandler, true);
// });
// body.addEventListener("click", clickHandler, false);

// list.addEventListener("click", clickHandler, true);

// list.addEventListener(
//    "click",
//    (evt) => {
//       console.log(`click on list`);
//       console.log(evt);
//       evt.stopPropagation();
//       evt.target.style.fontWeight = 700;
//    },
//    true
// );

// Array.from(items).forEach((el) => {
//    el.addEventListener(
//       "click",
//       (evt) => {
//          console.log(`click on item`);
//       },
//       true
//    );
// });

 