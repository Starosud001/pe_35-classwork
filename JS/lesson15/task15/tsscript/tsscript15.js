"use strict";
/**


Задача 1.


Необходимо «оживить» навигационное меню с помощью JavaScript.

При клике на элемент меню добавлять к нему CSS-класс .active.

Если такой класс уже существует на другом элементе меню, необходимо

с того, предыдущего элемента CSS-класс .active снять.

У каждый элемент меню — это ссылка, ведущая на google.

С помощью JavaScript необходимо предотвратить переход по всем ссылка на этот
внешний ресурс.

Условия:

В реализации обязательно использовать приём делегирования событий (на весь
скрипт слушатель должен быть один).
        <ul id="menu">
        <li><a href="#/php">PHP</a>
        <ul>
        <li><a href="#/php/manual">Справочник</a></li>
        <li><a href="#/php/snippets">Сниппеты</a></li>
        </ul>
        </li>
        <li><a href="#/html">HTML</a>
        <ul>
        <li><a href="#/html/information">Информация</a></li>
        <li><a href="#/html/examples">Примеры</a></li>
        </ul>
        </li>
        </ul>
*/
/*
const ul = document.querySelector("#menu");
const list = document.querySelectorAll("ul, li"); // все р в div
console.log(list);// 

ul.addEventListener('click', (event) =>{
    event.preventDefault() //отключаем стандртное действие(при нажатии на ссылку не преходить по ней)
    Array.from(list).forEach(element => {    
        element.classList.remove('active')    //при клике удаляем со всех элементов классы    
    }); 
        event.target.parentElement.classList.add('active')// после удаления добавляем в нужный эдемент
} )
*/
/*
Задача 2.

Есть список сообщений. Добавьте каждому сообщению по кнопке для его скрытия.
Картинка для кнопки удаления
*/

const list = document.getElementById('messages');


list.addEventListener('click',function(event) {
    if(event.target.closest(".close-btn")){
        event.target.closest(".pane").remove()
    }
})
