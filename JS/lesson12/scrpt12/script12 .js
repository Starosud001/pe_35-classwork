"use strict";
/*
const root = document.getElementById("root"); //получили id дива созданного раннее в HTML


 const paragraph = document.createElement("p");//           создали атрибут

 console.log(paragraph);

 paragraph.classList.add("text");//                         добавили класс   
 paragraph.style.fontSize = "30px";//                       добавили размер шрифта
 paragraph.textContent = "lorem ipsum sit amet";//          добавили текст
 paragraph.setAttribute("id", "big-text");//                добавили атрибут id

 paragraph.setAttribute("class", "big-text");//             добавили атрибут class

paragraph.setAttribute("data-number", "1");//               добавили пользовательский атрибут data-number
// // paragraph.setAttribute("data-size", "30px");
// paragraph.dataset.size = "30px";//                       тоже самое что и выше(добавление пользовательских атрибутов)

console.log(paragraph.getAttribute("id"));//                получить значение трибута id

*/
//root.append(paragraph); //        вставить внутри вконце
//root.prepend(paragraph);//        вставить внутри вконце
//root.after(paragraph);//          вставить снаружи после
// root.before(paragraph);//        вставить снаружи вконце

 //paragraph.remove();//              удалить элемент

//root.before("<strong>strong here</strong>");//всё будет вставлено в качестве текста будут отбражены и сами теги

//root.insertAdjacentHTML("afterbegin", "<strong>strong here</strong>");работает с текстом
//root.insertAdjacentHTML("afterbegin", paragraph);//     не работает с объектом
// root.insertAdjacentElement("afterbegin", paragraph);// работает тоько с объектами (нельзя 
//с помощью этого метода вставит простро текст  )
//root.insertAdjacentText("afterbegin", "лошпет");//работает с просто текстом

// paragraph.classList.add(); //        доваить класс
// paragraph.classList.remove();        удалить класс
// paragraph.classList.toggle(); //     метод в случае наличия класса удалит его, а в случае отсуствия - добавит
// console.log(paragraph.classList.contains("big-text"));// проверка существует  ли заданый класс


/*
 const fragment = document.createDocumentFragment();
 console.log(fragment);

 for (let index = 0; index < 5; index++) {
    const paragraph = document.createElement("p");
    paragraph.classList.add("text");//добавили класс
    paragraph.style.fontSize = "30px";//размер шрифта 
    paragraph.textContent = "lorem ipsum sit amet";//вставили текст

   fragment.append(paragraph);//вставили содержимое фрагмента вначало тега  <p>
 }
 console.log(fragment);

 root.append(fragment);
 console.log(fragment);
*/

