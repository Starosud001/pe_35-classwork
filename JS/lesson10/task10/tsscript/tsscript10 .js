"use strict";
/*
Задача 1.

Дан массив с днями недели.

Вывести в консоль все дни недели с помощью:

Классического цикла for;

Цикла for...of;

Специализированного метода для перебора элементов массива forEach. */


//const days = [ 'Monday', 'Tuesday', 'Wednesday', 'Thursday',
//'Friday', 'Saturday', 'Sunday', ];

/*
for (let index = 0; index < days.length; index++) {
    console.log (days[index]);
    console.log   
}
*/


/*
for (let iterator of days){
    console.log (iterator);
}
*/

/*
days.forEach((value, index, array) => {
    console.log(`${value}`);
})
*/


/*

Задача 2.

Написать мульти-язычную программу-органайзер.

Программа спрашивает у пользователя на каком языке он желает увидеть список
дней недели.
После ввода пользователем желаемого языка программа выводит в консоль дни
недели на указанном языке.

Доступные языки (локали): ua, ru, en.

Если введённая пользователем локаль не совпадает с доступными — программа
переспрашивает его до тех пор,
пока доступная локаль не будет введена.

Условия:

Решение должно быть коротким, лаконичным и универсальным;

Всячески использовать методы массива;

Использование метода Object.keys() обязательно. 

*/



const days = { 
    ua: [ 'Понеділок', 'Вівторок', 'Середа', 'Четвер','П’ятниця', 'Субота', 'Неділя', ], 
    ru: [ 'Понедельник', 'Вторник', 'Среда','Четверг', 'Пятница', 'Суббота', 'Воскресенье', ], 
    en: [ 'Monday', 'Tuesday','Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday', ], };

    //let getLang = prompt("введите язык")

    /*
    let getLang = prompt("введите язык")
    switch (getLang){
        case "ua" :
            console.log(days.ua);
            break;
            case "ru" :
                console.log(days.ru);
                break;
                case "en" :
                    console.log(days.en);
                    break;
    }
*/



/*
function setWeek () {
    let lang = "" ;
    while (!Object.keys(days).includes(lang)){
    lang = prompt("введите язык")
    }
    days[lang].forEach((element) => {
        console.log(element);
    };
    )   
}

setWeek();
*/



/*
Задача 3.

Напишите функцию mergeArrays для объединения нескольких массивов в один.

Функция обладает неограниченным количеством параметров.
Функция возвращает один массив, который является сборным из массивов,
переданных функции в качестве аргументов при её вызове.

Условия:

Все аргументы функции должны обладать типом «массив», иначе генерировать
ошибку;

В ошибке обязательно указать какой по счёту аргумент провоцирует ошибку.

Заметки:

Делать поддержку выравнивания вложенных массивов (флеттенинг) не нужно.
*/


function concatArray  (...params) {
    let newArr = [];
    params.forEach((element, index) => {
    if(!Array.isArray(params[index])){
        console.log(`Error in ${index} `); 
             
    }  
    })
    
    newArr += newArr.concat(...params);
    console.log(newArr)
 }
 
 concatArray([1,2,4], [1,2,3,54], 5);
