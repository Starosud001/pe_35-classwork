"use strict";

const arr = [1, 12, 33, 4, 65];
// console.log(arr);

// console.log(arr);
// console.log(typeof arr);

// arr.push(5, 5, 6);//             вставить в конец
// console.log(arr);

// arr.pop(); //                    удалить последний элемент
// console.log(arr);

// arr.unshift(5, 5, 6);//          добавить вначале
// console.log(arr);

// arr.shift();//                   удалить первый элемент
// console.log(arr);

//arr.reverse();
//console.log(arr);

//console.log("31.12.1999".split(".").reverse().join("."));//split(".") //разделит строку в элементы массива  join(".") - массив в строку

 //console.log(arr.length);
// console.log(arr[0]);

// console.log(arr.indexOf(12));//               выдаст//индекс этого числа в массиве
// console.log(arr.indexOf(2, 3));//             второй арг это стартовая позиция ,первый-искомое знач
// console.log(arr.lastIndexOf(2, 3));           тоже что и сверху но наоборот
// console.log(arr.includes(9));//               проверка на наличие искомого значения

// for (let index = 0; index < arr.length; index++) {
//    console.log(arr[index]);
// }

// for (const value of arr) {
//    console.log(value);
// }

// console.log(arr.concat([0, 9, 8], 100, 99, 88)); //      соберёт все значения в один массив
// console.log(arr);

// arr.length = 10; // длинна исходного массива с элементами = 5 задали длинну массива 10 (получили ещё : пустых слотов)
// console.log(arr); //заполнить:доп слоты с помщью push нельзя добавление пойдет после пустых то есть с 10 индекса (добавление возможно только по индексу)           

// arr[5] = 5;                   добавит с 10 с учётом верней операции           
// arr.push(5, 5, 5, 5, 5);

// arr[20] = 20;
// console.log(arr);

// delete arr[3];  //            получим при выводе значение - пустой слот, без смещения окружающих элементов
// console.log(arr);
//let res;

// res = arr.slice(2, 4); //     копирует часть массива(кусок в диапазоне заданых параметров) (без параметров скопирует всё)
// console.log(res);
// console.log(arr);

// res = arr.splice(2, 0, 9, 9, 9, 9, 9);//вырезает с исходного (изымая из него значения) 
//                                (аргументы после третьего будут вставляться на место вырезанных элементов в исходник)
// console.log(res);
// console.log(arr);

// function callBackFn(value, index, array) {           // создали функцию для перебора 
//    console.log(`${index}: ${value}, - ${array}`);
// }

//arr.forEach(callBackFn);                              //метод forEach  вызвали раннее созданую функцию callBackFn

// arr.forEach(function (value, index, array) {
//    console.log(`${index}: ${value}, - ${array}`);
// });

// arr.forEach((value, index, array) => {
//    console.log(`${index}: ${value}, - ${array}`);
// });

// arr.reverse();
// console.log(arr);

// arr.sort((a, b) => b - a);  //сортирует (по умолчанию - от меньшего к большему) но возможго настраивать 
// console.log(arr); 

const users = [
   {
      age: 33,
   },
   {
      age: 12,
   },
   {
      age: 18,
   },
   {
      age: 26,
   },
   {
      age: 30,
   },
   {
      age: 6,
   },
];

/*
let res = users.find((value, index, array) => value.age < 26);      //возвращает первое попавшееся значение  true
console.log(res);
*/

/*
let res = users.findIndex((value, index, array) => value.age > 30); //возвращает индекс первого попавшееся значение  true 

console.log(res);
*/


//let res = users.filter((value, index, array) => value.age > array.length);
//console.log(res);

/*
let res = users.map((value, index, array) => value.age + " years"); //создаёт из исходного массива новый, модифицированный
console.log(res);
*/

/*
let res = 0;
 users.forEach((el) => {
 res += el.age;
 });
 console.log(res);
*/

/*
let res = users.reduce(
    (startValue, currentValue, index, array) => startValue + currentValue.age, //возвращает сумму всех значений
    0
 );
 console.log(res);
*/

/*
let res = users.reduce(
    (startValue, currentValue, index, array) => startValue + currentValue.age,
    0
 );
 console.log(res);
*/

/*
 let res = users.reduceRight(
   (startValue, currentValue, index, array) => startValue + currentValue.age,
   0
);
console.log(res);
*/
