"use strict"

//Task1
/*
class Patient {
    constructor(height, weight) {
        this.height = height;
        this.weight = weight;
    }
    getPatientStatus() {
    let index = this.weight / (((this.height / 100) * this.height) / 100);
        //производим перевод сантиметров в метры
        index = Math.floor(index);
        if (index >= 10 && index <= 15) {
            return [index, `анорексия`];
        }
        if (index >= 16 && index <= 25) {
            return [index, `норма`];
        }
        if (index >= 26 && index <= 30) {
            return [index, `лишний вес`];
        }
        if (index >= 31 && index <= 35) {
            return [index, `I степень`];
        }
        if (index >= 36 && index <= 40) {
            return [index, `II степень`];
        }
        if (index > 41) {
            return [index, `III степень`];
        }
    }
}

const newPatient = new Patient(190, 80);
console.log(newPatient.getPatientStatus());
*/

//Task2
/*
class User {
    constructor(name, lastName, ...params) {
        this["name"] = name;
        this.lastName = lastName;
        this.params = {};

        params.forEach((el) => {
//пример элемента массива params "status: глава Юного клана Мескии",
            const [propName, propValue] = el.split(":");
            console.log(propName, propValue);
//мы будем делить элементы массива по ":" и записывать в [propName, propValue]
            this.params[propName] = propValue;
            console.log(propName, propValue);
        });
    }
}

 const user = new User(
     "Золар",
     "Аперкаль",
     "status: глава Юного клана Мескии",
//к этому мы применили выше const [propName, propValue] = el.split(":");
     "wife: Иврейн"
//к этому мы применили выше const [propName, propValue] = el.split(":");

 );
 console.log(user);
 //{name: 'Золар', lastName: 'Аперкаль', params: {…}}
//params: {status: ' глава Юного клана Мескии', wife: ' Иврейн'}
*/

//Task 3
/*
 class Warrior {
     constructor({ name, status, weapon }) {
//осуществили деструктуризацию в параметрах и извлекли нужные значения
         this.name = name;
         this.status = status;
         this.weapon = weapon;
     }
 }

 const warrior = new Warrior({
     name: "value",
     lastName: "value",
     weapon: "value",
     status: "value",
     vid: "value",
 });
 console.log(warrior);
 //warrior= {
 //   name: "value",
 //   weapon: "value",
 //   status: "value",
 //}
*/

/// TASK 4
/*
 const resume = {
     name: "Илья",
     lastName: "Куликов",
     age: 29,
     city: "Киев",
     skills: [
         { name: "Vanilla JS", practice: 5 },
         { name: "ES6", practice: 3 },
         {
             name: "React + Redux",
             practice: 1,
         },
         { name: "HTML4", practice: 6 },
         { name: "CSS2", practice: 6 },
     ],
 };

 const vacancy = {
     company: "SoftServe",
     location: "Киев",
     skills: [
         { name: "Vanilla JS", experience: 3 },
         { name: "ES6", experience: 2 },
         { name: "React + Redux", experience: 2 },
         { name: "HTML4", experience: 2 },
         { name: "CSS2", experience: 2 },
         { name: "HTML5", experience: 2 },
         { name: "CSS3", experience: 2 },
         { name: "AJAX", experience: 2 },
         { name: "Webpack", experience: 2 },
     ],
 };

 class Cv {
     constructor(resume, vacancy) {
         this.resume = resume;
         this.vacancy = vacancy;
//олучаем оюъекты в качестве параметров
     }

     checkPercent() {
         const { skills: resumeSkills } = resume;
//доста'м свойства skills с помощью деструктуризации и записываем в переменную
         const { skills: vacancySkills } = vacancy;

         let perc = 0;

         vacancySkills.forEach(({ name: vSName, experience: vSExp }) => {
проходимся по свойствам vacancySkills и записываем в переменную        
            let rSkill = resumeSkills.find(function ({ name: rSName }) {
                 return vSName === rSName; vacancySkills
//ищем соответствия значений свойств в  resumeSkills
             });

             if (rSkill) {
                 if (vSExp <= rSkill.practice) {
                     perc++;
                 }
             }
         });

         console.log(Math.floor((perc * 100) / vacancySkills.length));
     }//получаем процент совпавших значений свойств
 }

 const user = new Cv(resume, vacancy);
 user.checkPercent();
*/


// TASK 5
/*
 class MachineNumber {
     constructor(idMenu, selectorElem, idPlaceHolder) {
         this.idMenu = idMenu;
         this.selectorElem = selectorElem;
         this.idPlaceHolder = idPlaceHolder;
     }
     getNumber() {
         const menu = document.querySelector(`#${this.idMenu}`);
         console.log(menu)
//получаем  селектор по значению id полученому из параметров
         let menuNodes = [...menu.querySelectorAll(this.selectorElem)];
        console.log(menuNodes)
//разворачиваем меню и помещаем его элементы в массив  (теперь у элементов есть индексы)
         menu.addEventListener("click", function (event) {
             console.log(menuNodes.indexOf(event.target) + 1);
//поучаем индекс элемента на котором сработал  click  и добавляя +1 (получаем порядковый номер)
         });
     }
 }

 const obj = new MachineNumber("menu", ".war-machine", "war-machine-number");
 obj.getNumber();
*/

//Task 6

class Licence {
    constructor({ age: userAge, wantLicense: wantedLicence }) {
        this.userAge = userAge;
        this.licence = wantedLicence;
    }
    licenceCheck() {
        const driverLicence = [
            { age: 16, type: "A1", time: 2 },
            { age: 16, type: "A2", time: 2 },
            { age: 18, type: "B1", time: 6 },
            { age: 18, type: "B", time: 4 },
            { age: 19, type: "BE", time: 4 },
            { age: 21, type: "D1", time: 3 },
        ];

    }}

    
const user = { 
    age: 21,
    name: "Алексей", 
    lastName: "Михайлович", 
    wantLicense:"B1" 
}; 

