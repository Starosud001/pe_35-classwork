"use strict"
/*
 const arr = [1, "value 2", 3, 4, 5];

const [, secondValue, ...rest] = arr;
console.log(...arr)

//расскроет массив 1 'value 2' 3 4 5
console.log(secondValue, rest);
//value 2  [3, 4, 5]
*/

 
 const obj = {
     info: {
         name: "Uasya",
     },
     age: 18,
     count: 10,
 };
//создани объект

  const {
     age: userAge = 0,
     info: { name: userName = "" },
     count = 0,//
     //присвоили переменным значения
 } = obj;//объект из которого получаем свойства

 console.log(userAge, userName, count);
//18 'Uasya' 10

 console.log(obj);
 //info: {name: 'Uasya'}, age: 18, count: 10}

 const obj2 = { num: 1, ...obj, num2: 2 };
 obj2.count = 3;
 console.log(obj2);
 //{num: 1, info: {…}, age: 18, count: 3, num2: 2}




/*
 function fn(...rest) {
     //rest заворачивает в масси
     console.log(rest);
}
fn(1, 2, 3, 4, 5);
//[1, 2, 3, 4, 5]
 */