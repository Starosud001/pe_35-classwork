class Form {
    constructor(id, classes, action = "") {
        this.classes = classes;
        this.id = id;
        this.action = action;
        this._fields = this.createFields();
    }
    render() {
        let form = new Element(
            "form",
            this.id,
            this.classes,
            this._fields
        ).createElement();
        form.action = this.action;
        form.addEventListener("submit", this.handleSumbit.bind(this));

        return form;
    }
    handleSumbit(event) {
        event.preventDefault();
        let formInputs = [...event.target.elements];

        let errors = [];

        formInputs.forEach(function name(input, index, array) {
            if (index !== array.length - 1) {
                //  console.log(input);
                if (input.value.trim() === "") {
                    errors.push(input.name);
                    input.style.cssText = "outline: 2px solid #f00";
                }
            }
        });
        console.log(errors);
    }

    createFields() {
        return null;
    }
}

class RegisterForm extends Form {
    constructor(id, classes) {
        super(id, classes);
    }

    createFields() {
        let inputLogin = document.createElement("input");
        inputLogin.type = "text";
        inputLogin.name = "login";
        inputLogin.placeholder = "Your login";
        inputLogin.setAttribute("required", true);

        let inputEmail = document.createElement("input");
        inputEmail.type = "email";
        inputEmail.name = "email";
        inputEmail.placeholder = "Your email";
        inputEmail.setAttribute("required", true);

        let inputPass = document.createElement("input");
        inputPass.type = "password";
        inputPass.name = "password";
        inputPass.placeholder = "Your password";
        inputPass.setAttribute("required", true);

        let inputPassRepeat = document.createElement("input");
        inputPassRepeat.type = "password";
        inputPassRepeat.name = "repeat-password";
        inputPassRepeat.placeholder = "Repeat your password";
        inputPassRepeat.setAttribute("required", true);

        let inputSub = document.createElement("input");
        inputSub.type = "submit";
        inputSub.value = "registration";

        let fragment = document.createDocumentFragment();
        fragment.append(
            inputLogin,
            inputEmail,
            inputPass,
            inputPassRepeat,
            inputSub
        );
        return fragment;
    }
}

class LoginForm extends Form {
    constructor(id, classes) {
        super(id, classes);
    }
    createFields() {
        let inputLogin = document.createElement("input");
        inputLogin.type = "text";
        inputLogin.name = "login";
        inputLogin.placeholder = "Your login";
        inputLogin.setAttribute("required", true);

        let inputPass = document.createElement("input");
        inputPass.type = "password";
        inputPass.name = "password";
        inputPass.placeholder = "Your password";
        inputPass.setAttribute("required", true);

        let inputSub = document.createElement("input");
        inputSub.type = "submit";
        inputSub.value = "login";

        let fragment = document.createDocumentFragment();
        fragment.append(inputLogin, inputPass, inputSub);
        return fragment;
    }
}

class Element {
    constructor(tagName, id, classes, content, attributes) {
        this.tagName = tagName;
        this.id = id;
        this.classes = classes;
        this.content = content;
    }
    createElement() {
        let element = document.createElement(this.tagName);
        element.className = this.classes.join(" ");
        element.id = this.id;
        if (typeof this.content !== "object") {
            element.innerHTML = this.content;
        } else {
            element.append(this.content);
        }

        return element;
    }
}
//начало
class Modal {
    constructor(id, classes, text) {
        this.id = id;
        this.classes = classes;
        this.text = text;
        this._content = this.createContent();
    }
    createContent() {
        return "Modal window";
    }
    render(root = document.body) {
        let closeBtn = new Element(
            "span",
            "",
            ["close"],
            "&times;"
        ).createElement();

        let fragment = document.createDocumentFragment();
        fragment.append(closeBtn, this._content);

        let modalContent = new Element(
            "div",
            "",
            ["modal-content"],
            fragment
        ).createElement();

        let modal = new Element(
            "div",
            this.id,
            this.classes,
            modalContent
        ).createElement();

        this._modal = modal;
        root.append(modal);

        closeBtn.addEventListener("click", () => {
            modal.classList.remove("active");
        });
    }

    openModal() {
        this._modal.classList.add("active");
    }
}
const modalWindow = new Modal("id", ["modal", "modal1"], "Hello");
const root = document.querySelector("#root");
modalWindow.render(root);
const myBtn = document.querySelector("#myBtn");
myBtn.addEventListener("click", modalWindow.openModal.bind(modalWindow));

class Reg extends Modal {
    constructor(id, classes, text) {
        super(id, classes, text);
    }

    createContent() {
        let form = new RegisterForm("register-form", []).render();
        return form;
    }
}
const modalReg = new Reg("id", ["modal", "modal1"], "Hello");

modalReg.render(root);
const regBtn = document.querySelector("#registration");
regBtn.addEventListener("click", modalReg.openModal.bind(modalReg));

class Log extends Modal {
    constructor(id, classes, text) {
        super(id, classes, text);
    }

    createContent() {
        let form = new LoginForm("login-form", []).render();

        return form;
    }
}

const modalLog = new Log("id", ["modal", "modal1"], "Hello");
modalLog.render(root);
const logBtn = document.querySelector("#login");
logBtn.addEventListener("click", modalLog.openModal.bind(modalLog));
