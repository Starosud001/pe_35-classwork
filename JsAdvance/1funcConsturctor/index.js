"use strict"
/*
 const obj = {};

 const name = "Uasya";
 const age = 33;
 Object.create();

 function objectCreate(...params) {
     return { ...params };
 }

 console.log(objectCreate(name, age));
 */



/*
 function ObjectCreate (propName = "Empty", propAge = 18) {
   this.name = propName; //this. - ссылка на свойства будущего объекта
   this.age = propAge;
   this.showName = function () {  //создали метод для будущего объекта
   console.log(this.name);
   };
      let newAge = 33;
         const newName = "Petya";
         //return { //вся верхняя конструкция будет перекрыта из-за return(return -не должен быть внутри функции конструктора)
         //is: false,}
       
 }
 //вызов функции конструктора(на основе структуры функции ObjectCreate будет создан объект obj)
 const obj = new ObjectCreate("Uasya", 33); // выведет "Uasya", 33
 console.log(obj);
 obj.showName()//вызвали метод obj
 const obj2 = new ObjectCreate("Uasya2");//выведет  "Uasya" 18 (18 - значение поумолчанию )
 console.log(obj2);
*/



/*
//bind  call apply
//obj1
function ObjectCreate (propName = "Empty", propAge = 18) {
   this.name = propName; //this. - ссылка на свойства будущего объекта
   this.age = propAge;
   this.showName = function () {  //создали метод для будущего объекта
      console.log(this.name);}
}
const user = new ObjectCreate()
//obj2
const user1 = {
   name: "Stas"
}

 for (const key in user) {
//обращаемся к глобальному объекту Object, берём его метод hasOwnProperty и применяем на user 
//hasOwnProperty - проверяет существует ли свойство
     if (Object.hasOwnProperty.call(user, key)) {
         const element = user[key];
         console.log(element);
     }
   }

console.log(user);
//bind - создаёт новую  функцию с измененным this.
//c помощью  bind переадресовали this. метода showName в объекте user на объект user1
//user1.showName = user.showName.bind(user1)
//user1.showName()
//user.showName.bind(user1)(); //тоже самое что и в строках 57/58 (переадресовали this. на user1)

//call-возвращает ту же функцию с измененным this. (synt: fun.call(thisArg[, arg1[, arg2[, ...]]]))
//user.showName.call(user1)

//apply - возвращает ту же функцию с измененным this. (synt: fun.apply(thisArg, [argsArray]))
//user.showName.apply(user1)
*/

//Task1
/*
Напишите функцию-конструктор, создающую объект, реализующий такой
функционал: 
у нас на странице есть вопрос.
При первом клике на него под ним открывается ответ на вопрос. 
При повтором - ответ прячется. 
Разметку вы найдете в файле.

 <a href="" class="question">Девиз дома Баратеонов</a>
 <div id="root"> </div>

*/
/*
const questionText = "Девиз дома Баратеонов"; 
const questionAnswer = "Нам ярость!";
function Question (paramQuestion, paramAnswer){
   this.question = paramQuestion;
   this.answer = paramAnswer;


   this.render = function (rootElem = document.querySelector("#root")) {
      let link = document.createElement('a');
         link.classList.add('question');
         link.style.display = "block";
            link.textContent = this.question
            rootElem.append(link);
            let p = document.createElement('p');
               p.textContent = this.answer
                  p.style.display="none"
                  rootElem.append(p)
      link.addEventListener('click',function() {          
                  if(p.style.display=="none"){
                     p.style.display="block"
                     
                  }else {
                     p.style.display="none"
                  }
      }
      )
   }
}
const question1 = new Question(questionText, questionAnswer)
question1.render()
*/
/*
Task2
##TASK 2

Создайте функцию-конструтор объекта stopwatch согласно описанию:


Свойства объекта: - _time: время; 
- container: ссылка на DOM-элемент,внутри которого нужно выводить время.
Методы объекта: - start, stop, reset, работающие с его свойствами. -
setTime и getTime. setTimeбудет получать новое значение времени в
качестве аргумента, и проверять, является ли оно положительным целым числом.
Если да - то свойству _time будет присвоено значение аргумента, переданного
в метод setTime и метод вернет ответ объект вида: { status: "success", }


Если же аргумент не удовлетворяет критериям, то возвращать объект вида:
{
status: "error",
message: "argument must be positive integer"
}
Метод же getTime будет просто возвращать значение свойства _time для
использования его вне методов объектов.
<p id="time"></p>
<button id="start-time">Start</button>
<button id="stop-time">Stop</button>
<button id="reset-time">Reset</button>
function Stopwatch(container) {
}
*/

function Stopwatch(container) {
   this._time = 0;
   this._timer = null;
   this.container = container;

   this.start = function () {
       let start = 0;
       // const that = this;
       this._timer = setInterval(
           function () {
               // that.setTime(start++);
               this.setTime(start++);
               this.container.textContent = this.getTime();
           }.bind(this),
           100
       );
   };
   this.stop = function () {
       clearInterval(this._timer);
   };
   this.reset = function () {
       this.setTime(0);
       this.container.textContent = this.getTime();
   };
   this.setTime = function name(newTime) {
       if (newTime >= 0 && newTime % 1 === 0) {
           this._time = newTime;
           return {
               status: "success",
           };
       } else {
           return {
               status: "error",
               message: "argument must be positive integer",
           };
       }
   };
   this.getTime = () => this._time;
}
const startBtn = document.getElementById("start-time");
const stopBtn = document.getElementById("stop-time");
const resetBtn = document.getElementById("reset-time");

const stopWatchContainer = document.getElementById("time");
const stopwatch = new Stopwatch(stopWatchContainer);

startBtn.addEventListener("click", stopwatch.start.bind(stopwatch));
stopBtn.addEventListener("click", stopwatch.stop.bind(stopwatch));
resetBtn.addEventListener("click", stopwatch.reset.bind(stopwatch));
