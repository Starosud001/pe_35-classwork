// function User(name, age) {
//     this.name = name;
//     this.age = age;
// }

// const user = new User("Uasya", 18);
// // console.log(user);

// // console.log(user.__proto__);

// // console.log(User.prototype);

// // console.log(user.__proto__ === User.prototype);

// function showName() {
//     return this.name;
// }

// User.prototype.showName = showName;

// console.log(user.showName());
// console.log(user);

// const obj = new Object();
// console.log(typeof obj);
// console.log(obj instanceof Object);
// console.log(null instanceof Object);
// console.log(user instanceof User);

// function foo() {}
// console.log(foo);
// let bar = { a: "a4" };

// foo.prototype = bar;
// // Object {a: "a4"}
// const baz = Object.create(bar);
// // Object {a: "a4"}
// console.log(baz instanceof foo);
// // true. oops.
// console.log(bar instanceof foo);
// console.log(baz.__proto__ === foo.prototype);
// // true. oops.

// console.log(baz.__proto__);
// console.log(foo.prototype);
// console.log(bar);

// bar.count = 44;
// console.log(baz.__proto__);
// console.log(foo.prototype);
// console.log(typeof foo);

// class User {
//     constructor(name = "", age = 0) {
//         this.age = age;
//         this.name = name;
//     }
//     showName() {
//         return this.name;
//     }
//     checkStatus() {
//         return this.status || "guest";
//     }
//     set name(value) {
//         this._name = value;
//     }
// }

// const user = new User();
// // console.log(User);
// // console.log(user.checkStatus());
// console.log(user);

// class UserAdmin extends User {
//     constructor(name = "Uasya", age = 18, status = "") {
//         super(name, age);
//         this.status = status;
//         this.superPower = function () {
//             return "I'm Allmighty";
//         };
//     }
//     userHandler() {
//         return "Carry by admin";
//     }
// }

// const admin = new UserAdmin("Alex", 16, "Admin");
// console.log(admin);
// console.log(admin.checkStatus());
// console.log(admin.showName());

// class SubAdmin extends UserAdmin {}
// const subAdmin = new SubAdmin();

// console.log(subAdmin);
// console.log(subAdmin.checkStatus());
// console.log(subAdmin.showName());


// TASK 2
/*
class Element {
  //класс для создания элементов
  constructor(tagName, id, classes, content) {
      this.tagName = tagName;
      this.id = id;
      this.classes = classes;
      this.content = content;
  }
  createElement() {
    //метод класса  Element
      let element = document.createElement(this.tagName);
      element.className = this.classes.join(" ");
      element.id = this.id;
      if (typeof this.content !== "object") {
          element.innerHTML = this.content;
      } else {
          element.append(this.content);
      }

      return element;
  }
}

class Modal {
  constructor(id, classes, text) {
      this.id = id;
      this.classes = classes;
      this.text = text;
  }

  render(root = document.body) {
      //   let modal = document.createElement("div");
      //   modal.id = this.id;
      //   modal.className = this.classes.join(" ");
      //   let modalContent = document.createElement("div");
      //   modalContent.classList.add("modal-content");

      let p = new Element("p", "", [""], this.text).createElement();

      let closeBtn = new Element(
          "span",
          "",
          ["close"],
          "&times;"
      ).createElement();

      let fragment = document.createDocumentFragment();
      fragment.append(closeBtn, p);

      let modalContent = new Element(
          "div",
          "",
          ["modal-content"],
          fragment
      ).createElement();

      let modal = new Element(
          "div",
          this.id,
          this.classes,
          modalContent
      ).createElement();

      this._modal = modal;
      root.append(modal);

      closeBtn.addEventListener("click", () => {
          modal.classList.remove("active");
      });
  }

  openModal() {
      this._modal.classList.add("active");
  }
}
const modalWindow = new Modal("id", ["modal", "modal1"], "Hello");
const root = document.querySelector("#root");
modalWindow.render(root);
const myBtn = document.querySelector("#myBtn");
myBtn.addEventListener("click", modalWindow.openModal.bind(modalWindow));
