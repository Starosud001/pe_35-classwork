"use strict"
// task 1
// class Patient {
//     constructor(name, date, gender) {
//         this.name = name;
//         this.date = date;
//         this.gender = gender;
//     }
// }

// class PAtientCardio extends Patient {
//     constructor(name, date, gender, presure, problems) {
//         super(name, date, gender);
//         this.presure = presure;
//         this.problems = problems;
//     }
// }
// const patientCardio = new PAtientCardio(
//     "vasya",
//     18,
//     "man",
//     123,
//     "cardio problems"
// );
// console.log(patientCardio);

// class PAtientDantist extends Patient {
//     constructor(name, date, gender, dateVists, helth) {
//         super(name, date, gender);
//         this.dateVists = dateVists;
//         this.helth = helth;
//     }
// }
// const patientDantist = new PAtientDantist("vasya", 18, "man", 123, "gfg");
// console.log(patientDantist);


// TASK 2

class Element {
  //класс для создания элементов
  constructor(tagName, id, classes, content) {
      this.tagName = tagName;
      this.id = id;
      this.classes = classes;
      this.content = content;
  }
  createElement() {
    //метод класса  Element
      let element = document.createElement(this.tagName);
      element.className = this.classes.join(" ");
      element.id = this.id;
      if (typeof this.content !== "object") {
          element.innerHTML = this.content;
      } else {
          element.append(this.content);
      }

      return element;
  }
}

class Modal {
  constructor(id, classes, text) {
      this.id = id;
      this.classes = classes;
      this.text = text;
  }

  render(root = document.body) {
      //   let modal = document.createElement("div");
      //   modal.id = this.id;
      //   modal.className = this.classes.join(" ");
      //   let modalContent = document.createElement("div");
      //   modalContent.classList.add("modal-content");

      let p = new Element("p", "", [""], this.text).createElement();

      let closeBtn = new Element(
          "span",
          "",
          ["close"],
          "&times;"
      ).createElement();

      let fragment = document.createDocumentFragment();
      fragment.append(closeBtn, p);

      let modalContent = new Element(
          "div",
          "",
          ["modal-content"],
          fragment
      ).createElement();

      let modal = new Element(
          "div",
          this.id,
          this.classes,
          modalContent
      ).createElement();

      this._modal = modal;
      root.append(modal);

      closeBtn.addEventListener("click", () => {
          modal.classList.remove("active");
      });
  }

  openModal() {
      this._modal.classList.add("active");
  }
}
const modalWindow = new Modal("id", ["modal", "modal1"], "Hello");
const root = document.querySelector("#root");
modalWindow.render(root);
const myBtn = document.querySelector("#myBtn");
myBtn.addEventListener("click", modalWindow.openModal.bind(modalWindow));



