"use strict"
/*
 const posts = [
     { title: "First post", body: "This is first post" },
     { title: "Second post", body: "This is second post" },
 ];
 getPosts();

 function getPosts() {
     setTimeout(() => {
         posts.forEach((post, index) => {
             console.log(index + 1, post.title);
             console.log(post.body);
         });
     }, 1000);
 }
/*
 function createPost(post, callBack) {
     setTimeout(() => {
         posts.push(post);

         callBack();
     }, 2000);
 }

 //createPost({ title: "Third post", body: "This is third post" }, getPosts);

// createPost({ title: "Third post", body: "This is third post" }, function () {});

 //getPosts();
*/



// PROMISE
/*
 const posts = [
     { title: "First post", body: "This is first post" },
     { title: "Second post", body: "This is second post" },
 ];

 const promise = new Promise(function (resolve, reject) {
     try {
         //throw new Error("Our error");
  //создали Error//который прокидываем в catch
         setTimeout(() => {
             posts.push({ title: "Third post", body: "This is third post" });

             resolve(posts);
         }, 2000);
     } catch (e) {
            reject(new Error(e.message));//в случ ошибки
         //reject(e.message);//в случ ошибки
     }
 });
 console.log(promise);
 promise
     .then((data) => {
// .then срабатывает если нет ошибок
//колбек функция then получает в качестве аргумента тот что  пришло из resolve(45 стрка)
         data.forEach((post, index) => {
             console.log(index + 1, post.title);
             console.log(post.body);
         });
     })
//если в промисе сработал reject  то then не сработает переход будет на catch
     .catch((e) => {
          //  console.log(e.message);//в случ ошибки
         console.log(e);
     })
     .finally(() => {
         console.log("The end");
     });
//finally сработает в любом случае
*/



/*
 var p1 = Promise.resolve(3);
 //сразу вызываем resolve
 //Promise отработает и вернет значение (3)
 var p2 = 1337;

 var p3 = new Promise((resolve, reject) => {
     setTimeout(resolve, 100, "foo");
 });

 Promise.all([p1, p2, p3]).then((values) => {
     console.log(values);
//Promise.all возвращает  массивом значения из промисов
 });
*/


/////////////////////////////////////////////////////// 

/*
 const promise4 = new Promise(function (resolve, reject) {
     setTimeout(() => {
         resolve(4);
     }, 2000);
 });
// Good
 
  
promise4.then((data) => {
//создаём цепочку промисов
         return new Promise(function (resolve) {
             resolve(data);
         });
     })
     .then((data) => {
         console.log(data);
     });
//правильно это возвращать и определять по цепочке

 //Not good
 //закоментить  для запуска Good
 promise4.then((data) => {
     const promise5 = new Promise(function (resolve) {
         resolve(data);
     });
     promise5.then((data) => {
         console.log(data);
     });
 });
*/


/////////////////////////////////////


const BASE_URL = "https://ajax.test-danit.com/api/swapi/";
 const ENTITY = ["vehicles"];

 const getVehicles = new Promise((resolve, reject) => {
     const xhr = new XMLHttpRequest();
     xhr.open("GET", BASE_URL + ENTITY[0] + "/4");
     //собирается адрес
     xhr.send();

     xhr.onload = () => resolve(JSON.parse(xhr.response));
     //если запрос прошел успешно
     //xhr.onerror = () => reject(e);
     //запрос завершился крахом
 });

 getVehicles
     .then((response) => {
  //response получаем из  143 строки из resolve
         response = JSON.parse(response);
         console.log(response);
         console.log(response.consumables);
//         //   console.log(response[0]);
     })
     .catch((error) => {
         console.log(error);
     });

// const BASE_URL = "https://ajax.test-danit.com/api/swapi/";
// const ENTITY = ["vehicles"];
// const root = document.querySelector("#root");
// class Server {
//     constructor(url) {
//         this.url = url;
//     }
//     getData() {
//         const getVehicles = new Promise((resolve, reject) => {
//             const xhr = new XMLHttpRequest();
//             xhr.open("GET", this.url);
//             xhr.send();
//             xhr.onload = () => resolve(xhr.response);
//             xhr.onerror = () => reject(e);
//         });
//         let ul = document.createElement("ul");
//          //  let li = document.createElement("li");
//         getVehicles
//             .then((response) => {
//                 response = JSON.parse(response);
//                 console.log(response);
//                 for (let i = 0; i < response.length; i++) {
//                     let li = document.createElement("li");
//                     li.textContent = `${response[i].name}`;
//                     ul.append(li);
//                 }
//                 //   console.log(response[0]);
//             })
//             .catch((error) => {
//                 console.log(error);
//             });
//         root.append(ul);
//     }
// }
// const url = `https://ajax.test-danit.com/api/swapi/films/`;
// const getFilms = new Server(url);
// getFilms.getData();

// class Server {
//     constructor(url) {
//         this.url = url;
//     }

//     getData() {
//         const ENTITY = ["planets"];
//         const getVehicles = new Promise((resolve, reject) => {
//             const xhr = new XMLHttpRequest();
//             xhr.open("GET", this.url + ENTITY[0]);
//             xhr.send();
//             xhr.onload = () => resolve(xhr.response);
//             xhr.onerror = () => reject(e);
//         });
//         let ul = document.createElement("ul");

//         getVehicles
//             .then((response) => {
//                 response = JSON.parse(response);
//                 console.log(response);

//  let newarr = response.sort(function (a, b) {
//      if (b.diameter > a.diameter) {
//          return 1;
//      }
//  });
//  console.log(newarr);

//  for (let i = 0; i < response.length; i++) {
//      let li = document.createElement("li");
//      li.textContent = `${response[i].name} - ${response[i].diameter}`;
//      ul.append(li);
//  }
//             })
//             .catch((error) => {
//                 console.log(error);
//             });
//         root.append(ul);
//     }
// }

// const planets = new Server("https://ajax.test-danit.com/api/swapi/");
// planets.getData();

// const BASE_URL = "https://ajax.test-danit.com/api/swapi/";
// const ENTITY = ["vehicles"];
// const root = document.querySelector("#root");
// class Server {
//     constructor(url) {
//         this.url = url;
//     }
//     getData() {
//         const getVehicles = new Promise((resolve, reject) => {
//             const xhr = new XMLHttpRequest();
//             xhr.open("GET", this.url);
//             xhr.send();
//             xhr.onload = () => resolve(xhr.response);
//             xhr.onerror = () => reject(e);
//         });
//         let ul = document.createElement("ul");
//          //  let li = document.createElement("li");
//         getVehicles
//             .then((response) => {
//                 response = JSON.parse(response);
//                 console.log(response);
//                 for (let i = 0; i < response.length; i++) {
//                     let li = document.createElement("li");
//                     li.textContent = `${response[i].name}`;
//                     ul.append(li);
//                 }
//                 //   console.log(response[0]);
//             })
//             .catch((error) => {
//                 console.log(error);
//             });
//         root.append(ul);
//     }
// }
// const url = `https://ajax.test-danit.com/api/swapi/films/`;
// const getFilms = new Server(url);
// getFilms.getData();
