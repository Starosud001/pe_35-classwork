/* let a = 3;

 function fn() {
    return a + 3; 
    //значение а взято из глобальной облисти 
 }
*/

/*
 function fnWrapper() {
     let a = 3;

     return function fnInner() {
         return a + 3;
         //значение а взято из контекстной облисти видимости
         //область ограничевается фигурными  {}
     };
 }
 */

// function summ(a, b, c) {
//     return a + b + c;
// }
// summ(a, b, c)


/*
 function summ(a) {
     return function (b) {
         return function (c) {
             return a + b + c;
         };
     };
 }
 summ(a)(b)(c);
const sumTwoAndTwoAnd = summ(2)(2);
console.log(sumTwoAndTwoAnd(3))
будет зафиксировано значение (2)(2) и они станут наччальными про последующих вызовах
*/


/*
 const baadJson = "{bad json}";

 function parseJson(json) {
     try {
         return JSON.parse(json);
     } catch (e) {
         console.dir(e.message);
     } finally {
         //finally срабатывает всегда
         console.log("Working always");
     }
 }

 console.log(parseJson(baadJson));
 console.log(parseJson(JSON.stringify({ a: 3 })));
 console.log("hello");
*/


/*
 function toUpp(str) {
     try {
         return str.toUpperCase();
     } catch (e) {
         console.dir(e);
         console.dir(e.name);
         throw new Error(e.message);
     }
 }

 let str = "fghjkl";
 let upperStr;

 try {
     upperStr = toUpp(null);
 } catch (e) {
     console.log(e.message);
     upperStr = str;
 }

 console.log(upperStr);
*/