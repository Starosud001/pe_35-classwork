"use strict"

//TASK1
/*
class Change {
         constructor(dom, sizePlus, sizeMinus) {
             this.dom = dom;
             this.sizePlus = sizePlus;
             this.sizeMinus = sizeMinus;
         }
    
         plusSize() {
             let sizeText = parseInt(this.dom.style.fontSize);
//получили значение fontSize
             sizeText += this.sizePlus;
//прибавили значение из параметра sizePlus
             this.dom.style.fontSize = `${sizeText}px`;
//переписали исходное значение fontSize
             console.log(sizeText);
         }
         minusSize() {
             let sizeText = parseInt(this.dom.style.fontSize);
//получили значение fontSize
             sizeText -= this.sizeMinus;
//прибавили значение из параметра sizeMinus
             this.dom.style.fontSize = `${sizeText}px`;
//переписали исходное значение fontSize
             console.log(sizeText);
         }
     }
    
     const p = document.querySelector("#text");
     //получили селектор по айди
     const change = new Change(p, 2, 3);
//передали параметры в класс chenge(dom, sizePlus, sizeMinus)
    
     const btnSizePlus = document.querySelector("#increase-text");
     btnSizePlus.addEventListener("click", change.plusSize.bind(change));
//с помощью bind  применили метод plusSize на объекте change
     const btnSizeMinus = document.querySelector("#decrease-text");
     btnSizeMinus.addEventListener("click", change.minusSize.bind(change));
//с помощью bind  применили метод minusSize на объекте change
*/


/*
//TASK2
     class СreateProduct{
        constructor(name, fullName, article, price) {
        this.name = name;
        this.fullName = fullName;
        this.article = article;
        Object.defineProperty(this, "price", {
//определяет новые или изменяет существующие свойства
        get: function () {
            return price;
         },
         set: function (newPrice) {
            if (newPrice > 0 && newPrice % 1 === 0) {
                price = newPrice;
            }
          },
        });
    }//constructor
}//class
            
        

     

 const notebook = new СreateProduct(
//вызвали конструктор объекта и сразу передали параметры (name, fullName, article, price)
     "lenovo X120S",
     "lenovo X120S(432-44) W",
     3332,
     23244
 );

console.log(notebook.price); // выведет 23244
notebook.price = -4; // присвоение не произойдет
//из-за действия Object.defineProperty function (newPrice)
console.log(notebook.price); //выведет 23244
notebook.price = 22000;
console.log(notebook.price); // выведет 22000
*/

//TASK 3

class Slider {
    constructor(effect, speed, autostart, slides) {
        this.effect = effect;
        this.speed = speed;
        this.autostart = autostart;
        this.slides = [...slides];
    }
    addSlide(slide) {
        if (
            typeof slide === "object" &&
            slide !== null &&
            !Array.isArray(slide)
        ) {//проверка на соответсве полученного в качестве слайда параметра у метода addSlide
            this.slides.push(slide);
            //если всё ок то пушим slide в slides
        } else {
            console.log("error");
        }
    }

    getSlide(order) {
        try {
            if (this.slides[order - 1]) {
                return this.slides[order - 1];
            }
            return new Error("No such slider");
        } catch (e) {
            // throw new Error(e.message);
        }
    }


    deleteSlide(number) {
        if (number < this.slides.length) {
            console.log(this.slides.length);
            this.slides.splice(number - 1, 1);
    //второй аргумент в splice (количество элементов для удаления)
        }
    }
}
try {
    const slides = [{ id: 1 }, { id: 2 }, { id: 3 }, { id: 4 }, { id: 5 }];

    const slider = new Slider("fade", 100, true, slides);
    slider.addSlide({ id: 6 });
    slider.deleteSlide(3);
    slider.deleteSlide(6);

    slides.push("ee");
    console.log(slider.slides);

    console.log(slider.getSlide(0));
    console.log(slider.getSlide(4));
} catch (e) {
    console.log(e.message);
}


