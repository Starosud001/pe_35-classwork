const { serv } = require('./serv.js');
const { watcher } = require('./watch.js');
const { scripts } = require ('./scripts.js');
const { styles } = require('./style.js');
const { images } = require('./images.js');

module.exports = {
    serv,
    watcher,
    scripts,
    styles,
    images
};
