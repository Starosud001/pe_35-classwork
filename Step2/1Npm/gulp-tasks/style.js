const {src, dest} = require ("gulp");
const sass = require('sass');
const gulpSass = require('gulp-sass')(sass);
const {bs} = require ("./serv.js")
const sourcemaps =require("gulp-sourcemaps")


function buildStyles() {
    src("./src/scss/**/*.scss")
    .pipe(sourcemaps.init())
    .pipe(gulpSass().on("error", gulpSass.logError))
    .pipe(sourcemaps.write())
    .pipe(dest("./dist/css"))
    .pipe(bs.reload({ stream: true }));

    };

exports.styles = buildStyles